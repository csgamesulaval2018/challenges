# Puzzle Heroes

## Le nettoyant Vim rend la cuisine propre à nouveau après la réalisation d'une recette chaotique
 
En se servant de l'éditeur de texte Vim, vous devez transformer la recette chaotique `epreuve.txt` et la rendre identique à `recette.txt`.

### Contraintes

Vous ne devez utiliser que des commandes **Ex**, c.-à-d. celles qui commencent par `:`, tel que `:w`, `:g`, `:q`, `:pu`, etc.

De plus, vous devez y parvenir en utilisant **5 commandes ou moins**.

Inscrivez vos commandes ci-dessous dans l'ordre d'exécution. Vous pouvez assumer que tous les registres de l'instance de Vim qu'utiliseront les correcteurs seront vides avant de tester vos commandes.

### Réponses

#### 1ère commande

: …

#### 2e commande

: …

#### 3e commande

: …

#### 4e commande

: …

#### 5e commande

: …

Bonne chance!
