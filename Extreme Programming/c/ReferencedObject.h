#ifndef REFERENCEDOBJECT_H_
#define REFERENCEDOBJECT_H_

/**
 * A little bit of information on reference counting, a way to do OO in C.
 *
 * This module is to be added as
 *    THE VERY FIRST element of
 *    EACH module that is meant
 *    to be an "ReferencedObject" proper.
 *
 * Usage: 
 *    - A module's referencedObject is to be created in its constructor
 *      (the [ModuleName]_new(...) method).
 *    - The method "ReferencedObject_reference(...)" is to be called for the referencedObject
 *      of each module that is received in another module (the owner of) in its constructor's parameters.
 *    - At the beginning of a module's destructor (the [ModuleName]_delete(...) method), the method
 *      "ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(...)" is to be called in order to 
 *      know if it must be freed.
 *
 */

struct ReferencedObject {
    int reference_count;
};

struct ReferencedObject *ReferencedObject_new(void);
void ReferencedObject_reference(struct ReferencedObject *referencedObject);
_Bool ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(struct ReferencedObject *referencedObject);

#endif 

