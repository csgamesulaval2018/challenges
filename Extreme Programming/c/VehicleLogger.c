#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "VehicleLogger.h"

static struct VehicleLogger *logger;

/*

Insert any static functions you want here

*/

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands,
                                        struct ReferencedObject *vehicleReferencedObject)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    FILE *newLogFile = fopen("log.txt", "a+");

    setbuf(newLogFile, NULL);
    fprintf(newLogFile, "\n\n------------ NEW EXECUTION ------------\n\n");

    struct VehicleLogger *pointer = malloc(sizeof(struct VehicleLogger));

    ReferencedObject_reference(vehicleReferencedObject);
    pointer->vehicleReferencedObject = vehicleReferencedObject;
    pointer->referencedObject = newReferencedObject;
    pointer->logFile = newLogFile;

    /* assign them using the vehicleCommands parameter

    pointer->originalVehicleCommands;
    pointer->actualVehicleCommands;

    */

    logger = pointer;

    return pointer;
}

void VehicleLogger_delete(struct VehicleLogger *logger)
{
    stopLogging();

    if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->referencedObject)) {
        fprintf(logger->logFile, "\n\n------------ END OF EXECUTION ------------\n\n");
        fclose(logger->logFile);

        if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->vehicleReferencedObject)) {
            free(logger->vehicleReferencedObject);
        }

        free(logger);
    }
}

void startLogging(void)
{
    /*
        Complete me: once called, calls to the Vehicle methods (e.g. HondaCivic_setSpeed) should log into log.txt
    */
}

void stopLogging(void)
{
    /*
        Complete me: once called, calls to the Vehicle methods should work normally (no logging)
    */
}
