# Mobile

## Fait dont tu beau au Québec?

### Introduction
Pour réussir ce challenge vous devez faire une application Android simple qui communique avec un API de météo afin d'afficher la température d'une ville de votre choix.

### Buts
* Construire une application android fonctionelle
* Utiliser des bonnes pratiques de programmation
* Communiquer avec un API externe

Chaque sous-partie vaut des points. Par exemple, vous pouvez vous concentrer sur le fonctionnement de l'application en hardcodant des données directement et revenir à la fin pour faire le call d'API si vous avez le temps. L'important est que vous en fassiez le plus possible pour aller chercher un maximum de points.

### Fonctionnalités demandées
* Une vue par ville
  * Affiche le nom de la ville
  * Affiche la température actuelle
  * Affiche le maximum et le minimum pour la journée
  * Une image représentant la condition météorologique actuelle
* L'application doit être esthétique
* Bonus
  * Possiblité d'avoir la météo pour plusieurs villes et d'interchanger de vues entre chacune d'elles
  * Animations
  * Publicités payantes

### API de météo
Vous pouvez utiliser le service que vous voulez pour aller chercher la météo. En voici quelques exemples:
* MetaWeather: (Recommandé: Pas besoin de s'inscrire ni d'avoir une clé d'API. Cependant, ne contient pas toutes les villes) https://www.metaweather.com/api/
* AccuWeather: http://apidev.accuweather.com/developers/api-quick-start-guides
* OpenWeatherMap: https://openweathermap.org/appid

#### MetaWeather flow
Pour trouver la température pour une ville avec l'API de MetaWeather, il vous faut deux appels.
* **Trouver le woeid** (Where on Earth ID): https://www.metaweather.com/api/location/search/?query=montréal
```
Réponse de l'API:
[
    {
      title: "Montréal",
      location_type: "City",
      woeid: 3534,
      latt_long: "45.512402,-73.554680"
    }
]
```

* **Trouver la météo pour une date précise à partir du woeid**: https://www.metaweather.com/api/location/{woeid}/{année}/{mois}/{jour}/
```
Réponse de l'API:
[
    {
      id: 5358007575642112,
      weather_state_name: "Showers",
      weather_state_abbr: "s",
      wind_direction_compass: "WNW",
      created: "2017-11-06T23:42:42.539230Z",
      applicable_date: "2017-11-06",
      min_temp: 0.3960000000000001,
      max_temp: 13.866,
      the_temp: 13.66,
      wind_speed: 6.1687799587624275,
      wind_direction: 291.80442776338765,
      air_pressure: 1010.905,
      humidity: 78,
      visibility: 13.870247753121768,
      predictability: 73
    },
    {
      id: 5218891169005568,
      weather_state_name: "Showers",
      weather_state_abbr: "s",
      wind_direction_compass: "WNW",
      created: "2017-11-06T20:42:41.469060Z",
      applicable_date: "2017-11-06",
      ...
    },
    ...
]
```
Allez chercher `the_temp` dans le premier objet de la liste pour avoir la dernière mise à jour de la température pour cette journée.

* **Trouver une icône**  
À partir de `weather_state_abbr`, vous pouvez aller chercher une image qui représente la météo actuelle à partir de MetaWeather. https://www.metaweather.com/static/img/weather/{weather_state_abbr}.svg

### Modalités de remise
Vous devez remettre un fichier `meteo.apk` au même niveau que ce README dans ce répertoire. Votre application sera exécutée dans un émulateur.

Bonne chance!